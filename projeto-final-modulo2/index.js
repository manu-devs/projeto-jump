let boneco = document.querySelector("#boneco");
let obstaculo = document.querySelector("#obstaculo");
let cont = 0;

function pular() {
  cont = cont + 1;
  if (boneco.classList != "animar") {
    boneco.classList.add("animar");
  }

  setTimeout(function () {
    boneco.classList.remove("animar");
  }, 500);
}

let verificarBatida = setInterval(function () {
  let topoBoneco = parseInt(
    window.getComputedStyle(boneco).getPropertyValue("top")
  );
  let esquerdaObstaculo = parseInt(
    window.getComputedStyle(obstaculo).getPropertyValue("left")
  );

  if (esquerdaObstaculo < 20 && esquerdaObstaculo > 0 && topoBoneco >= 130) {
    obstaculo.style.animation = "none";
    obstaculo.style.display = "none";
    alert(`Game Over! Pontução: ${cont} pontos`);
  }
}, 10);
